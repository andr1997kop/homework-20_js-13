
const start = document.querySelector(`.start`);
const stop = document.querySelector(`.stop`);
const resume = document.querySelector(`.resume`);

const images = document.querySelectorAll(`img`);


let currentSlide = 0;

let slider;


function imageShow() {
  images[currentSlide].classList = `image-to-show`;
  currentSlide = (currentSlide + 1) % images.length;
  images[currentSlide].classList = `image-to-show active`;
};

start.addEventListener(`click`, () => {
  slider = setInterval(imageShow, 3000);
  stop.classList = `btn stop active`;
  resume.classList = `btn continue active`;
});

stop.addEventListener(`click`, () => {
  clearInterval(slider);
})

resume.addEventListener(`click`, () => {
  slider = setInterval(imageShow, 3000);
  });



